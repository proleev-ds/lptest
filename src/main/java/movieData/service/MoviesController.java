package movieData.service;

import movieData.objectModel.Genre;
import movieData.objectModel.Movie;
import movieData.objectModel.MovieFilter;
import movieData.provider.themoviedb.ServiceProvider;
import org.codehaus.stax2.ri.typed.StringBase64Decoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@RestController
public class MoviesController {
    @Autowired
    ServiceProvider service;

    @RequestMapping("/")
    public String about() {
        return "This is movieData test service. Go to /genres firstly.";
    }

    @RequestMapping(path = "/genres",
            method = RequestMethod.GET)
    public Genre[] getGenres() {
        return service.getGenres();
    }

    @RequestMapping(path = "/movies", method = RequestMethod.GET)
    public Movie[] getMovies(
            @RequestHeader(value = "minVoteCount", required = false, defaultValue = "0") int minVoteCount,
            @RequestHeader(value = "maxVoteCount", required = false, defaultValue = "0") int maxVoteCount,
            @RequestHeader(value = "minVoteAvg", required = false, defaultValue = "0.0f") float minVoteAvg,
            @RequestHeader(value = "maxVoteAvg", required = false, defaultValue = "0.0f") float maxVoteAvg,
            @RequestHeader(value = "genres", required = false) String genres,
            @RequestHeader(value = "year", required = false, defaultValue = "0") int year,
            @RequestHeader(value = "sortBy", required = false) String sortBy,
            @RequestHeader(value = "maxResult", defaultValue = "1000") int maxResult) {
        MovieFilter filter = new MovieFilter()
                .setMinVoteCount(minVoteCount)
                .setMaxVoteCount(maxVoteCount)
                .setMinVoteAvg(minVoteAvg)
                .setMaxVoteAvg(maxVoteAvg)
                .setGenres(genres)
                .setYear(year);
        return service.getMovies(filter, sortBy, maxResult);
    }

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.GET)
    public Movie getTheMovie(
            @PathVariable(value = "id") int id) {
        return service.getTheMovie(id);
    }

    @RequestMapping(path = "/genreVoteAvg", method = RequestMethod.GET)
    public String getGenreVoteAvg(@RequestHeader(value = "genreId") int genreId) {
        return service.calculateGenreVoteAvg(genreId);
    }

    @RequestMapping(path = "/genreVoteAvgState", method = RequestMethod.GET)
    public String getGenreVoteAvgResult(@RequestHeader(value = "genreId") int genreId) {
        return service.getGenreVoteAvg(genreId);
    }
}
