package movieData.provider;

import movieData.objectModel.Genre;
import movieData.objectModel.Movie;
import movieData.objectModel.MovieFilter;

import java.util.List;

public interface IServiceProvider {
    Genre[] getGenres();
    Movie[] getMovies(MovieFilter filter, String movieSorting, int maxResult);
    Movie getTheMovie(int id);
    String calculateGenreVoteAvg(int genreId);
    String getGenreVoteAvg(int genreId);
}
