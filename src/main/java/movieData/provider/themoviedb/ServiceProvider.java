package movieData.provider.themoviedb;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.ir.Expression;
import jdk.nashorn.internal.runtime.Context;
import movieData.objectModel.Genre;
import movieData.objectModel.Movie;
import movieData.objectModel.MovieFilter;
import movieData.provider.IServiceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

@Component
public class ServiceProvider implements IServiceProvider {
    public static String serviceURL = "http://api.themoviedb.org/3/{req}" +
            "?api_key=72b56103e43843412a992a8d64bf96e9";
    public static String GET_GENRES_METHOD = "genre/movie/list";
    public static String GET_MOVIES_METHOD = "discover/movie";
    public static String GET_THE_MOVIE_METHOD = "movie/";
    public static String GET_MOVIES_BY_GENRE_METHOD = "discover/movie";


    public static final int MAX_PAGE_COUNT = 1000;

    private static Requestor requestor = new Requestor();
    //private RestTemplate client = new RestTemplate();

    @Override
    public Genre[] getGenres() {
        JsonNode jsonNode = requestor.getRestTemplate().getForObject(serviceURL, JsonNode.class, GET_GENRES_METHOD);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(jsonNode.get("genres").toString(), Genre[].class);
        } catch (IOException e) {
            return new Genre[0];
        }
    }

    @Override
    public Movie[] getMovies(MovieFilter filter, String movieSorting, int maxResult) {
        StringBuilder methodURL = new StringBuilder(serviceURL);
        methodURL.append(MovieFilterHelper.getFilterAsURLPart(filter));
        if (movieSorting != null)
            methodURL.append("&sort_by" + movieSorting);
        ResponseEntity<JsonNode> response = requestor.getRestTemplate().exchange(methodURL.toString(),
                HttpMethod.GET, null, JsonNode.class, GET_MOVIES_METHOD);

        JsonNode responseBody = response.getBody();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            Movie[] movies = mapper.readValue(responseBody.get("results").toString(), Movie[].class);
            if (movies.length > maxResult)
                return movies;
            else {
                int pageCount = mapper.readValue(responseBody.get("total_pages").toString(), Integer.class);
                if (pageCount == 1)
                    return movies;

                HashSet<Movie> results = new HashSet<Movie>(Arrays.asList(movies));

                int pageIndex = 1;
                while (pageIndex < pageCount && pageIndex <= MAX_PAGE_COUNT && results.size() < maxResult) {
                    response = requestor.getRestTemplate().exchange(methodURL.toString() + "&page=" + Integer.toString(++pageIndex),
                            HttpMethod.GET, null, JsonNode.class, GET_MOVIES_METHOD);
                    results.addAll(Arrays.asList(mapper.readValue(response.getBody().get("results").toString(), Movie[].class)));
                }
                return results.toArray(new Movie[results.size()]);
            }
        } catch (IOException e) {
            return new Movie[0];
        }
    }

    @Override
    public Movie getTheMovie(int id) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return mapper.readValue(requestor.getRestTemplate().getForObject(serviceURL, String.class, GET_THE_MOVIE_METHOD + Integer.toString(id)), Movie.class);
        } catch (IOException e) {
            Context.printStackTrace(e);
            return null;
        }
    }

    private HashMap<Integer, Double> genreVoteAvgStates = new HashMap<>();

    private HashMap<Integer, Double> genreVoteAvgs = new HashMap<>();

    @Override
    public String calculateGenreVoteAvg(final int genreId) {
        if (genreVoteAvgs.containsKey(genreId))
            return "vote average has been calculated. call /genreVoteAvgState";
        else if (genreVoteAvgStates.containsKey(genreId))
            return "calculating vote average have already started.";
        else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (genreVoteAvgStates.putIfAbsent(genreId, 0.0d) == null)
                    {
                        Deque<Float> results = new ArrayDeque<Float>();
                        ResponseEntity<JsonNode> response = requestor.getRestTemplate().exchange(serviceURL + "&with_genres=" + Integer.toString(genreId),
                                HttpMethod.GET, null, JsonNode.class, GET_MOVIES_BY_GENRE_METHOD);

                        JsonNode responseBody = response.getBody();
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                        try {
                            Movie[] movies = mapper.readValue(responseBody.get("results").toString(), Movie[].class);
                            for (Movie movie : movies)
                                results.add(movie.getVoteAvg());

                            int pageCount = mapper.readValue(responseBody.get("total_pages").toString(), Integer.class);
                            if (pageCount > 1) {
                                int pageIndex = 1;
                                genreVoteAvgStates.put(genreId, (double) pageIndex / pageCount * 100);
                                while (pageIndex < pageCount && pageIndex <= MAX_PAGE_COUNT) {
                                    response = requestor.getRestTemplate().exchange(serviceURL
                                                    + "&with_genres=" + Integer.toString(genreId)
                                                    + "&page=" + Integer.toString(++pageIndex),
                                            HttpMethod.GET, null, JsonNode.class, GET_MOVIES_BY_GENRE_METHOD);
                                    movies = mapper.readValue(response.getBody().get("results").toString(), Movie[].class);
                                    for (Movie movie : movies)
                                        results.add(movie.getVoteAvg());
                                    genreVoteAvgStates.put(genreId, (double) pageIndex / pageCount * 100);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        double sum = 0.0d;
                        for (float voteAvg : results)
                            sum += voteAvg;
                        if (genreVoteAvgs.putIfAbsent(genreId, (double) sum / results.size()) == null)
                            genreVoteAvgStates.remove(genreId);
                    }
                }
            }).start();
            return "calculating vote average started.";
        }
    }
    public String getGenreVoteAvg(int genreId) {
        if (genreVoteAvgStates.containsKey(genreId))
            return new StringBuilder("calculating... %").append(Double.toString(genreVoteAvgStates.get(genreId))).toString();
        else if (genreVoteAvgs.containsKey(genreId))
            return new StringBuilder("genre vote average is ").append(Double.toString(genreVoteAvgs.get(genreId))).toString();
        else
            return "call /genreVoteAvg firstly";
    }
}
