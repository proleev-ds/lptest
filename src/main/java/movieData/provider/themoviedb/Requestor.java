package movieData.provider.themoviedb;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

public class Requestor {
    private static final int COOLDOWN_TIMEOUT = 300;
    private static final Object lock = new Object();
    private static boolean isServiceAvailable = true;

    private static final RestTemplate restTemplate = new RestTemplate();

    private void coolDown() {
        synchronized (lock) {
            try {
                Thread.sleep(COOLDOWN_TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            isServiceAvailable = true;
            lock.notifyAll();
        }
    }

    public RestTemplate getRestTemplate() {
        synchronized (lock) {
            while (!isServiceAvailable) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            isServiceAvailable = false;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    coolDown();
                }}).start();
            return restTemplate;
        }

    }
}
