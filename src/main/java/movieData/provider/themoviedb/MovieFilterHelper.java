package movieData.provider.themoviedb;

import movieData.objectModel.MovieFilter;

public class MovieFilterHelper {
    public static String getFilterAsURLPart(MovieFilter filter){
        if (filter != null) {
            StringBuilder result = new StringBuilder();
            if (filter.getYear() > 0)
                result.append("&year=" + Integer.toString(filter.getYear()));
            if (filter.getGenres() != null)
                result.append("&with_genres=" + filter.getGenres());
            if (filter.getMinVoteAvg() > 0)
                result.append("&vote_average.gte=" + Float.toString(filter.getMinVoteAvg()));
            if (filter.getMaxVoteAvg() > 0)
                result.append("&vote_average.lte=" + Float.toString(filter.getMaxVoteAvg()));
            if (filter.getMinVoteCount() > 0)
                result.append("&vote_count.gte=" + Integer.toString(filter.getMinVoteCount()));
            if (filter.getMaxVoteCount() > 0)
                result.append("&vote_count.lte=" + Integer.toString(filter.getMaxVoteCount()));
            return result.toString();
        }
        else return "";
    }
}
