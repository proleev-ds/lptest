package movieData.objectModel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Movie {
    @JsonProperty("id")
    private int id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("genre_ids")
    private int[] genre_ids;
    @JsonProperty("genres")
    private Genre[] genres;
    @JsonProperty("overview")
    private String overview;
    @JsonProperty("vote_average")
    private float voteAvg;
    @JsonProperty("vote_count")
    private int voteCount;

    public float getVoteAvg() {
        return voteAvg;
    }
}
