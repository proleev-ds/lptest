package movieData.objectModel;

public class MovieFilter {
    private int minVoteCount;
    private int maxVoteCount;
    private float minVoteAvg;
    private float maxVoteAvg;
    private String genres;
    private int year;

    public int getMinVoteCount() {
        return minVoteCount;
    }

    public MovieFilter setMinVoteCount(int minVoteCount) {
        this.minVoteCount = minVoteCount;
        return this;
    }

    public int getMaxVoteCount() {
        return maxVoteCount;
    }

    public MovieFilter setMaxVoteCount(int maxVoteCount) {
        this.maxVoteCount = maxVoteCount;
        return this;
    }

    public float getMinVoteAvg() {
        return minVoteAvg;
    }

    public MovieFilter setMinVoteAvg(float minVoteAvg) {
        this.minVoteAvg = minVoteAvg;
        return this;
    }

    public float getMaxVoteAvg() {
        return maxVoteAvg;
    }

    public MovieFilter setMaxVoteAvg(float maxVoteAvg) {
        this.maxVoteAvg = maxVoteAvg;
        return this;
    }

    public String getGenres() {
        return genres;
    }

    public MovieFilter setGenres(String genres) {
        this.genres = genres;
        return this;
    }

    public int getYear() {
        return year;
    }

    public MovieFilter setYear(int year) {
        this.year = year;
        return this;
    }
}
